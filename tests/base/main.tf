variable "pet_count" {
  default = 3
}
variable "prefix" { 
  default = {
    default = "Jerri Bear",
    workspace-dev = "Fletcher",
    workspace-stage = "Gebo"
    workspace-prod = "Daisey" 
    }
}

resource "random_pet" "my_pet" {
  count     = var.pet_count
  separator = " "
  length    = "3"
  prefix    = var.prefix[terraform.workspace]
}

output "pet_out" {
  value = random_pet.my_pet[*].id
}

terraform{
  backend "local" {}
}